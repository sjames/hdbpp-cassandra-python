# hdbpp-python (WIP/PROTOTYPE)

This is a prototype API to access the Cassandra HDB++ database via a python API. This is NOT a completed project yet.

## TODO

* Throttle the process/threads a little (overloads the cluster for sprectrums currently, this may be solved by a change in schema I am looking at)
* Handled failed connections better
* Adapt the request a bit better for spectrums
* That annoying hardcoded timezone in Attribute.get_data_last_period()
* General tidy up
* Move to github

## Cloning

```
git clone --recurse-submodules git clone git@gitlab.esrf.fr:sjames/hdbpp-cassandra-python.git
```

## Building and Installation

### Dependencies

The project has the following general dependencies.

* Minimum python version: 2.7.9

### Python Module Dependencies

* cassandra-driver
* pandas

Various packages in use in the test file:

* bokeh
* guppy
* memory_profiler

Simple install all via pip:

```bash
pip install cassandra-driver pandas
```

If trying to run the test file:

```bash
pip install bokeh guppy memory_profiler
```