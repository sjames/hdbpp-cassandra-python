#!/usr/bin/env python
# 
import cProfile, pstats
import memory_profiler
import socket
import itertools

import pandas

from guppy import hpy
import hdbpp

from bokeh.plotting import figure, output_file, show
from bokeh.models import ColumnDataSource, HoverTool

def run():
    hp = hpy()

    cs_name = "orion.esrf.fr:10000"
    #cs_name = "acu-cassandra.esrf.fr:10000"
    #contact_points = [ "acu-cassandra" ]
    contact_points = [ "hdbr1", "hdbr2", "hdbr3" ]
    contact_ips = [socket.gethostbyname(host) for host in contact_points]
    keyspace = "hdb"

    connection = hdbpp.Connection(contact_ips, keyspace)

    attr = hdbpp.create_attribute(connection, cs_name, "sr/d-bpm/all", "sa" )
    #attr = hdbpp.create_attribute(connection, cs_name, "sr/d-bpm/d6", "zemg" )    
    #attr = hdbpp.create_attribute(connection, cs_name, "sy/ms/1", "frequency" )

    rows = 0

    pr = cProfile.Profile()
    pr.enable()

    result = []

    for data in hdbpp.get_data_last_period(attr, days=20, data_time_long=False, error_desc=False, quality=False):
        result.append(data)
        #result = list(itertools.chain(*[result, data]))

#        if len(data) > 0:
 #           for d in data:
  #              result.append(d)
   #             rows += d.shape[0]
                #print "D", d

    #print "result", result
    result = pandas.concat(result, ignore_index=True)
    #print "result now", result
    result.set_index("data_time", inplace=True)
    #print "result now", result

    pr.disable()
    ps = pstats.Stats(pr).sort_stats("cumulative")
    ps.print_stats(0.01)

    #print hp.heap()
    #print hp.heap().size / 1000000, "MB"

    #result.index = pandas.to_datetime(result.index)
    #result.index.name = "Date"

    source = ColumnDataSource(data=result)

    output_file("test.html")

    hover = HoverTool(
        tooltips=[
            ("Date", "@data_time{%F %T}"),
            ("Value", "$y")
        ],
        formatters={
            "data_time" : "datetime"
        }    
    )

    p = figure(
        title="Test Plot", 
        x_axis_type="datetime", 
        x_axis_label="Event Time", 
        y_axis_label="Value")

    p.add_tools(hover)

    p.line(x="data_time", y="value_r", legend="sy/ms/1/frequency", source=source)
    show(p)



    #result.resample("T", on="data_time").sum()
    #print "REULT", result


if __name__ == "__main__":
    #run()
    mem_usage = memory_profiler.memory_usage(run)
    print "Memory usage (at 0.1 seconds): %s", mem_usage
    print "Max memory %s" % max(mem_usage)

