#!/usr/bin/env python

from __future__ import print_function

import pandas

# Import elements of the Datastax Cassandra Python Driver
import cassandra
import cassandra.cluster
import cassandra.query
import cassandra.policies

# import the package settings file
import hdbpp_settings
from hdbpp_exceptions import ClusterConnectionFailed


def pandas_factory(colnames, rows):
    if len(rows) == 0:
        return pandas.DataFrame()

    return pandas.DataFrame(rows, columns=colnames)


class CassandraConnection:
    """ Used to establish and manage a connection to the cassandra database.

    Raises:
        ClusterConnectionFailed -- [description]
    """

    def __init__(self, addresses, keyspace):
        self.addresses = addresses
        self.keyspace = keyspace
        self.prepared_statements = {}

        self.cluster = None
        self.session = None

        self.cluster = cassandra.cluster.Cluster(
            self.addresses, load_balancing_policy=cassandra.policies.DCAwareRoundRobinPolicy())

        try:
            self.session = self.cluster.connect(keyspace=keyspace)

        except cassandra.cluster.NoHostAvailable as e:
            raise ClusterConnectionFailed("Unable to connect to Cassandra Cluster", e.errors)

        if hdbpp_settings.verbose:
            print("Created cassandra connection on ", self.addresses)

    def __del__(self):
        """ Cleanly shutdown the connection to the database.
        """

        if self.session is not None:
            self.session.shutdown()

        if self.cluster is not None:
            self.cluster.shutdown()

        if hdbpp_settings.verbose:
            print("Destroyed cassandra connection on ", self.addresses)

    def get_prepared_statement(self, query):
        """ All queries are cached as prepared statements, and this function either
            creates or retrieves the statement for the Attribute.

        Arguments:
            query {[type]} -- [description]

        Returns:
            [type] -- [description]
        """

        # check for a cached statement, if none exists, create
        # a new one and cache it
        if query in self.prepared_statements:
            return self.prepared_statements[query]

        statement = self.session.prepare(query)
        self.prepared_statements[query] = statement

        if hdbpp_settings.verbose:
            print("Cached a prepared statement for query:", query)

        return statement

    def set_profile_general(self):
        self.cluster.default_retry_policy = cassandra.policies.FallthroughRetryPolicy()
        self.session.row_factory = cassandra.query.dict_factory
        self.session.default_consistency_level = cassandra.ConsistencyLevel.LOCAL_QUORUM
        self.session.default_timeout = 20

    def set_profile_worker(self):
        self.cluster.default_retry_policy = cassandra.policies.DowngradingConsistencyRetryPolicy()
        self.session.row_factory = pandas_factory
        self.session.default_consistency_level = cassandra.ConsistencyLevel.LOCAL_QUORUM
        self.session.default_timeout = 60
