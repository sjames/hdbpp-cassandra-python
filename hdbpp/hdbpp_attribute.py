#!/usr/bin/env python

from __future__ import print_function

import pytz

from datetime import datetime
from datetime import timedelta
from time import strftime

# import the package files
import hdbpp_settings
import hdbpp_cassandra
import hdbpp_query_manager

from hdbpp_exceptions import AttributeNameQuery
from hdbpp_exceptions import InvalidTimeRange


def build_id_query():
    """ Return a query string to request the attribute id information from the database. 
        This includes information such as attribute id and data type.

    Returns:
        string -- The completed query string
    """
    return "SELECT att_conf_id, data_type FROM att_conf WHERE cs_name = ? AND att_name = ? LIMIT 1"


def build_data_query_for_period(
        table_name, data_time=False, data_time_us=False, error_desc=False, quality=False, value_r=False, value_w=False):
    """ Construct a query string for a period in time. This is less optimal than a day long query,
        but may be required when trying to split the work of a large query down across multiple
        processes and threads.

    Arguments:
        table_name {string} -- Table name to query

    Keyword Arguments:
        data_time {bool} -- Add data_time to query (default: {False})
        data_time_us {bool} -- Add data_time_us to query (default: {False})
        error_desc {bool} -- Add error_desc to query (default: {False})
        quality {bool} -- Add quality to query (default: {False})
        value_r {bool} -- Add value_r to query (default: {False})
        value_w {bool} -- Add value_w to query (default: {False})

    Returns:
        string -- The completed query string
    """

    query = build_data_query_for_day(table_name, data_time, data_time_us, error_desc, quality, value_r, value_w)
    query += "AND data_time >= ? AND data_time < ?"
    return query


def build_data_query_for_day(
        table_name, data_time=False, data_time_us=False, error_desc=False, quality=False, value_r=False, value_w=False):
    """ Construct a query string to request one days worth of data from the database. 
        This is an optimal period, since scalars are stored per day per partition.

    Arguments:
        table_name {string} -- Table name to query

    Keyword Arguments:
        data_time {bool} -- Add data_time to query (default: {False})
        data_time_us {bool} -- Add data_time_us to query (default: {False})
        error_desc {bool} -- Add error_desc to query (default: {False})
        quality {bool} -- Add quality to query (default: {False})
        value_r {bool} -- Add value_r to query (default: {False})
        value_w {bool} -- Add value_w to query (default: {False})

    Returns:
        string -- The completed query string
    """

    def add_selected_field_to_query(field_name, field_list):
        """ Auxiliary function for build_data_query_for_day. Add a field 
            to the query list

        Arguments:
            field_name {string} -- Field name to add
            field_list {list} -- List to add the field to
        """

        if len(field_list) > 0:
            field_list.append(", ")

        field_list.append(field_name)

    query_builder = []
    query = "SELECT "

    # Add any selected fields to the query
    if data_time is True:
        add_selected_field_to_query("data_time", query_builder)

    if data_time_us is True:
        add_selected_field_to_query("data_time_us", query_builder)

    if error_desc is True:
        add_selected_field_to_query("error_desc", query_builder)

    if quality is True:
        add_selected_field_to_query("quality", query_builder)

    if value_r is True:
        add_selected_field_to_query("value_r", query_builder)

    if value_w is True:
        add_selected_field_to_query("value_w", query_builder)

    # Loop on the list adding fields to the query, we expect it to be correctly
    # formatted by this point
    for field in query_builder:
        query += field

    query += " FROM " + table_name + " WHERE att_conf_id = ? AND period = ? "
    return query


def time_to_aligned_chunks(start, period_delta, chunk_size):
    """ Generator function to break down the period into chunks aligned to
        the chunk size (days, hours, minutes).

    Arguments:
        start {timedate} -- Start time for the period
        period_delta {timedelta} -- Total time of the period
        chunk_size {timedelta} -- The time chunk to try break down the period into

    Returns:
        timedate, timedate, timedate -- Date, start_time and end_time
    """

    def get_next_day(current):
        """ Gets the next day aligned start point

        Arguments:
            current {timedate} -- Current timedate

        Returns:
            timedate -- Next start time
        """

        next_time = current + timedelta(days=1)

        # check for day boundary
        if next_time.replace(hour=0, minute=0, second=0, microsecond=0) > current.replace(hour=0, minute=0, second=0, microsecond=0):
            next_time = next_time.replace(hour=0, minute=0, second=0, microsecond=0)

        return next_time

    def get_next_hour(current):
        """ Gets the next hour aligned start point

        Arguments:
            current {timedate} -- Current timedate

        Returns:
            timedate -- Next start time
        """

        next_time = current + timedelta(hours=1)

        # check for hour boundary
        if next_time.replace(minute=0, second=0, microsecond=0) > current.replace(minute=0, second=0, microsecond=0):
            next_time = next_time.replace(minute=0, second=0, microsecond=0)

        return next_time

    end = start + period_delta
    current = start
    next_time_func = None

    if chunk_size == timedelta(days=1):
        next_time_func = get_next_day
    else:
        next_time_func = get_next_hour

    next_time = next_time_func(current)

    # Yield the results as they are calculated
    while next_time < end:
        yield (current.date(), current, next_time)

        current = next_time
        next_time = next_time_func(next_time)

    # if a remainder exists, yield it
    if current <= end:
        yield (current.date(), current, end)


def get_results(attr, start_time, delta,
                data=True, data_time=True, data_time_long=False, error_desc=False, quality=False):
    """ Using the start time and delta, attempt to query the database for the given attribute.

    Arguments:
        attr {Attribute} -- Attribute to fetch results for
        start_time {timedate} -- Start time of the query
        delta {timedelta} -- Period to retrieve data for

    Keyword Arguments:
        data {bool} -- Request values from database (default: {True})
        data_time {bool} -- Request data event time from database (default: {True})
        data_time_long {bool} -- Request data event time in long from database (default: {False})
        error_desc {bool} -- Request error description from database (default: {False})
        quality {bool} -- Request quality from database (default: {False})
    """

    if hdbpp_settings.verbose:
        print("Request data from: ", start_time, " for period ", delta)

    query_parameters = []

    jobs = 0
    chunk_size = None

    # spectrum's place a burden on the cluster, especially if they have been recorded
    # at high intervals, so we reduce the request size. It will take longer to read
    # from the database, but its more likely to succeed
    if attr.is_spectrum() is True:
        chunk_size = hdbpp_settings.spectrum_job_chunk_size

        if hdbpp_settings.verbose:
            print("Attribute is spectrum, so chunksize set to:", chunk_size)

    else:
        chunk_size = hdbpp_settings.scalar_job_chunk_size

        if hdbpp_settings.verbose:
            print("Attribute is scalar, so chunksize set to:", chunk_size)

    # Take the time period and break it down into chucks aligned to either a day or
    # hour, depending on whether its a scalar or spectrum. This builds a list of jobs
    # give to the query manager, which will distribute the query across the cpu's
    for date, start_time, end_time in time_to_aligned_chunks(start_time, delta, chunk_size):

        # when using a 1 day chunk size, we use a slightly different query, and no longer need
        # the start and end times
        if chunk_size == timedelta(days=1):
            query_parameters.append((attr.id, strftime("%Y-%m-%d", date.timetuple())))
        else:
            query_parameters.append((attr.id, strftime("%Y-%m-%d", date.timetuple()), start_time, end_time))

        jobs += 1

        if hdbpp_settings.verbose:
            print("Added job", jobs, "for time period on date:", date, "start time:", start_time, "end_time", end_time)

    # determine whether to request the value data based on the parameters
    value_r = False
    value_w = False

    if data is True:
        if attr.is_read_only() is True:
            value_r = True
            value_w = False

        elif attr.is_read_write() is True:
            value_r = True
            value_w = False

    query_string = None

    # ensure we catch a 1 day chunk size with the better query string
    if chunk_size == timedelta(days=1):
        query_string = build_data_query_for_day(
            attr.table_name, data_time, data_time_long, error_desc, quality, value_r, value_w)

    else:
        query_string = build_data_query_for_period(
            attr.table_name, data_time, data_time_long, error_desc, quality, value_r, value_w)

    if hdbpp_settings.verbose:
        print("Build query set to:", query_string)

    # Allocate a query manager to manage this request to the database, it will spread
    # the load across several cpu's
    query_manager = hdbpp_query_manager.QueryManager(attr.connection, hdbpp_settings.cpu_count_config)

    for query_result in query_manager.run_query(query_string, query_parameters):
        yield query_result

    query_manager.close_pool()


def create_attribute(connection, control_system, device_name, attribute_name):
    """ Creates an Attribute object that can be used to request data from the 
        database with.

    Arguments:
        connection {Connection} -- Connection details to reach the cassandra database
        control_system {string} -- Name of the control system and port, i.e: cs.system:10000
        device_name {[type]} -- The device name as domain/family/member
        attribute_name {[type]} -- The attribute name on the device

    Raises:
        AttributeNameQuery -- Raised when the device can not be found in the database
    """

    # create a connection to the cassandra database to look up the device in
    connection = hdbpp_cassandra.CassandraConnection(connection.addresses, connection.keyspace)
    connection.set_profile_general()

    fqdn_attribute_name = device_name + "/" + attribute_name

    # attempt to query the database about the device
    prepared = connection.get_prepared_statement(build_id_query())
    bound_statement = prepared.bind((control_system, fqdn_attribute_name))
    future = connection.session.execute_async(bound_statement)

    try:
        row = future.result()
    except Exception as e:
        raise AttributeNameQuery("Error fetching the attributes id and type")

    # the query is set to return a single result, so we access the first element
    # of the result only
    id = row[0]["att_conf_id"]
    data_type = row[0]["data_type"]
    table_name = "att_" + data_type

    # allocate the attribute object
    attribute = Attribute(connection, id, data_type, table_name)

    if hdbpp_settings.verbose:
        print("Created attribute with id:", attribute.id,
              "data type:", attribute.data_type, "and table:", attribute.table_name)

    return attribute


def get_data_last_period(attr, weeks=0, days=0, hours=0, minutes=0, seconds=0,
                         data=True, data_time=True, data_time_long=False, error_desc=False, quality=False):
    """ Generator function that returns data as it becomes ready. User must either collect
        the data in a separate frame, or better, use it as its returned to reduce the
        memory footprint of the call. Making large requests may be throttled by the API.
        Requests are based on the last period defined by the parameters.

    Arguments:
        attr {Attribute} -- Attribute to fetch results for

    Keyword Arguments:
        weeks {int} -- Last weeks of data to request (default: {0})
        days {int} -- Last weeks of data to request (default: {0})
        hours {int} -- Last weeks of data to request (default: {0})
        minutes {int} -- Last weeks of data to request (default: {0})
        seconds {int} -- Last weeks of data to request (default: {0})
        data {bool} -- Request values from database (default: {True})
        data_time {bool} -- Request data event time from database (default: {True})
        data_time_long {bool} -- Request data event time in long from database (default: {False})
        error_desc {bool} -- Request error description from database (default: {False})
        quality {bool} -- Request quality from database (default: {False})
    """

    date = datetime.now(pytz.timezone("Europe/Paris")).replace(microsecond=0)
    #date = datetime.now(pytz.utc).replace(microsecond=0)
    delta = timedelta(weeks=weeks, days=days, hours=hours, minutes=minutes, seconds=seconds)

    start_time = date - delta

    # yield results back to the user as they are returned from the query
    for result in get_results(attr, start_time, delta,
                              data=data, data_time=data_time,
                              data_time_long=data_time_long,
                              error_desc=error_desc, quality=quality):
        yield result


def get_data_in_range(attr, start_time, end_time,
                      data=True, data_time=True, data_time_long=False, error_desc=False, quality=False):
    """ Generator function that returns data as it becomes ready. User must either collect
        the data in a separate frame, or better, use it as its returned to reduce the
        memory footprint of the call. Making large requests may be throttled by the API.
        Requests are made in the range start - end.

    Arguments:
        attr {Attribute} -- Attribute to fetch results for
        start_time {datetime} -- Data request start time
        end_time {datetime} -- Data request end time

    Keyword Arguments:
        data {bool} -- Request values from database (default: {True})
        data_time {bool} -- Request data event time from database (default: {True})
        data_time_long {bool} -- Request data event time in long from database (default: {False})
        error_desc {bool} -- Request error description from database (default: {False})
        quality {bool} -- Request quality from database (default: {False})

    Raises:
        InvalidTimeRange -- On an invalid time range
    """

    delta = end_time - start_time

    if delta < 0 or end_time > datetime.now() or start_time > datetime.now():
        raise InvalidTimeRange("Invalid range request %s to %s" % (start_time, end_time))

    # yield results back to the user as they are returned from the query
    for result in get_results(attr, start_time, delta,
                              data=data, data_time=data_time,
                              data_time_long=data_time_long,
                              error_desc=error_desc, quality=quality):
        yield result


class Attribute:
    def __init__(self, connection, id, data_type, table_name):
        self.id = id
        self.data_type = data_type
        self.table_name = table_name
        self.connection = connection

    def is_scalar(self):
        """ Returns True if the attribute is a scalar

        Returns:
            bool -- Returns True if this attributes is a scalar, False otherwise
        """

        if "scalar" in self.data_type:
            return True

        return False

    def is_spectrum(self):
        """ Returns True if the attribute is a spectrum

        Returns:
            bool -- Returns True if this attributes is a spectrum, False otherwise
        """

        return not self.is_scalar()

    def is_read_only(self):
        """ Returns True if the attribute access is read only

        Returns:
            bool -- Returns True if this attributes access is read only, False otherwise
        """

        if "_ro" in self.data_type:
            return True

        return False

    def is_read_write(self):
        """ Returns True if the attribute access is read/write

        Returns:
            bool -- Returns True if this attributes access is read/write, False otherwise
        """

        if "_rw" in self.data_type:
            return True

        return False


class Connection:
    """ Small class used to describe a database connection by the user. 
    """

    def __init__(self, addresses, keyspace="hdb"):
        self.addresses = addresses
        self.keyspace = keyspace
