#!/usr/bin/env python
from datetime import timedelta

verbose = True

cpu_count_config = 10
query_concurrency = 20
process_chunk_size = 100

scalar_fetch_size = None
spectrum_fetch_size = None

scalar_job_chunk_size = timedelta(days=1)
spectrum_job_chunk_size = timedelta(hours=1)
