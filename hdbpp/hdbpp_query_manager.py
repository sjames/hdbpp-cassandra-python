#!/usr/bin/env python

from __future__ import print_function
import os
import math

import pandas

from multiprocessing import Pool, cpu_count
from functools import partial
from cassandra.concurrent import execute_concurrent_with_args
from cassandra import ReadTimeout

# import the package settings file
import hdbpp_settings
import hdbpp_cassandra


class QueryManager(object):
    """ Class to handle the query process to the database. This class decides the
        number of processes and threads to spread the units of work across.
    """

    def __init__(self, connection, process_count=None):
        # The query managers thread pool, each worker takes a basic connection
        # to create its down cassandra connection from
        self.pool = Pool(processes=process_count, initializer=self._setup_workers, initargs=(connection,))

    @classmethod
    def _setup_workers(cls, connection):
        """ Class method to setup the worker process cassandra configuration

        Arguments:
            connection {Connection} -- A small class describing the cassandra connection.
        """

        # Use the connection class to create a cassandra connection that is
        cls.connection = hdbpp_cassandra.CassandraConnection(connection.addresses, connection.keyspace)
        cls.connection.set_profile_worker()

    def close_pool(self):
        """ Close the process pool and wait for all the processes to finish
        """

        self.pool.close()
        self.pool.join()

    def run_query(self, query, query_params):
        """ Request the query manager run a query. This will be split across multiple 
            processes and threads in an attempt to speed up the request. The function is a
            generator, and will return results as they are returned from the database.

        Arguments:
            query_params {list} -- [description]
            query {string} -- The actual query string

        Returns:
            list -- A list of results
        """

        # Set to a default, before we try balance the load across the cpu's
        # a little better
        chunksize = hdbpp_settings.process_chunk_size

        # Attempt to balance smaller counts across the cpu's, to even the distribution and
        # take full advantage of the cpu count. Each worker will execute up to self.concurrency
        # threads in a single shot
        if (len(query_params) / hdbpp_settings.cpu_count_config) < hdbpp_settings.process_chunk_size:
            chunksize = int(math.ceil(float(len(query_params)) / float(hdbpp_settings.cpu_count_config)))

        if hdbpp_settings.verbose:
            print("Selected thread chunksize as:", chunksize, "across:", hdbpp_settings.cpu_count_config, "cpu's")

        # Pass the query request to the worker pool, breaking it down into chunks based
        # on the concurrency count above. Yield the results back to the user as they come
        # in, to reduce memory footprint and allow process to begin. Note we expect results as a
        # list of results, and return a single set of results at a time
        for query_results_list in self.pool.imap(
                partial(_multiprocess_get, query=query, concurrency=hdbpp_settings.query_concurrency),
                (query_params[n:n + chunksize] for n in xrange(0, len(query_params), chunksize))):

            if query_results_list is not None:
                for query_result in query_results_list:
                    yield query_result

    @classmethod
    def _execute_query(cls, query_params, query, concurrency):
        """ Execute the query via the Datastax Python API

        Arguments:
            query_params {list} -- [description]
            query {string} -- The actual query string

        Returns:
            list of list -- List of results from the query
        """

        if hdbpp_settings.verbose:
            print("Process", os.getpid(), "received", len(query_params), "requests to process")

        # get the prepared statement for this query
        prepared = cls.connection.get_prepared_statement(query)

        results = None
        attempts = 1

        # sometimes we get timeouts with rthe requests, this is due to the cluster not being able
        # to service the size of the requests, to mitigate this, we make up to three request,
        # and add a random back off for each failure
        while attempts < 4 and results is None:
            try:
                # Execute the requests concurrently via the cassandra python driver. Results are
                # passed back as a list of lists, the query manager will split these down and
                # return a list for each iteration
                result = [results[1]._current_rows for results in
                          execute_concurrent_with_args(cls.connection.session, prepared, query_params, concurrency)]

            except ReadTimeout as e:
                if hdbpp_settings.verbose:
                    print("Caught a ReadTimeout on attempt", attempts, ", trying again...")
                    print("Exception:", e)

                attempts = attempts + 1

        if results is None:
            print("Partial query failure due to timeout. Try a smaller date range to reduce the load on the cluster")

        return result


def _multiprocess_get(query_params, query, concurrency):
    """ Call into the QueryManager class methods to perform the database query

    Arguments:
        query_params {list} -- List of parameters to bind to the query
        query {string} -- The actual query string

    Returns:
        list of list -- List of results from the query
    """
    return QueryManager._execute_query(query_params, query, concurrency)
