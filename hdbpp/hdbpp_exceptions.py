#!/usr/bin/env python


class ClusterConnectionFailed(Exception):
    """[summary]
    """

    def __init__(self, message, driver_error_map):
        super(ClusterConnectionFailed, self).__init__(message)
        self.driver_error_map = driver_error_map


class AttributeNameQuery(Exception):
    """[summary]
    """
    pass


class InvalidTimeRange(Exception):
    """ Raised when an invalid data period is requested from the Attribute 
        class.
    """
    pass
